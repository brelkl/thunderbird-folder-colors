# Folder Colors for Thunderbird


## License

Licensed under the Mozilla Public License Version 2.0 (MPL 2.0). Please see the LICENSE file, or refer to <https://www.mozilla.org/en-US/MPL/2.0/>.


## Release Notes

**Effective end-of-life as of TB78:** This add-on is superseded in TB78 by the built-in folder-color changer (in folder > 'Properties').


### v1.0.0

* Forked/developed from:  
  *Color Folders v1.1, by fisheater*  
  *Colored Folders v1.2, by Lab5*

* Renamed to 'Folder Colors' to avoid overlap and confusion.

* Removed 'Custom colors...' option from the menu because I couldn't get the new color picker working (at this time).

* Changed the default folder colors.

* Changed the access key to 'f' to avoid overlap.

* Simplified much of the code to improve maintainability.


### v1.0.1

* Updated TB minimum version (to 60.0).

* Fixed some spelling mistakes.
